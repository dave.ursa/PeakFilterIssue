/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"


//==============================================================================
PkFilterTestAudioProcessor::PkFilterTestAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", AudioChannelSet::stereo(), true)
                     #endif
                       )
#endif
{
	addParameter(processSingle = new AudioParameterBool("processSingle", "ProcessSingleSampleRaw", false));

}

PkFilterTestAudioProcessor::~PkFilterTestAudioProcessor()
{
}

//==============================================================================
const String PkFilterTestAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool PkFilterTestAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool PkFilterTestAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

double PkFilterTestAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int PkFilterTestAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int PkFilterTestAudioProcessor::getCurrentProgram()
{
    return 0;
}

void PkFilterTestAudioProcessor::setCurrentProgram (int index)
{
}

const String PkFilterTestAudioProcessor::getProgramName (int index)
{
    return String();
}

void PkFilterTestAudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void PkFilterTestAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
}

void PkFilterTestAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool PkFilterTestAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    if (layouts.getMainOutputChannelSet() != AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void PkFilterTestAudioProcessor::processBlock(AudioSampleBuffer& buffer, MidiBuffer& midiMessages)
{
	const int totalNumInputChannels = getTotalNumInputChannels();
	const int totalNumOutputChannels = getTotalNumOutputChannels();

	// In case we have more outputs than inputs, this code clears any output
	// channels that didn't contain input data, (because these aren't
	// guaranteed to be empty - they may contain garbage).
	// This is here to avoid people getting screaming feedback
	// when they first compile a plugin, but obviously you don't need to keep
	// this code if your algorithm always overwrites all the output channels.
	for (int i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
		buffer.clear(i, 0, buffer.getNumSamples());

	float cut = Decibels::decibelsToGain(-24.0f);
	//convert bandwidth in #octaves into Q
	float affectedOctaves = 0.66f;
	float twoToTheWidth = pow(2, affectedOctaves);
	float q = pow(twoToTheWidth, 0.5) / (twoToTheWidth - 1);

	filterAPeak.setCoefficients(IIRCoefficients::makePeakFilter(getSampleRate(), 2500.0f, q, cut));
	filterBPeak.setCoefficients(IIRCoefficients::makePeakFilter(getSampleRate(), 2500.0f, q, cut));

	// This is the place where you'd normally do the guts of your plugin's
	// audio processing...

	float* leftData = buffer.getWritePointer(0);
	float* rightData = buffer.getWritePointer(1);

	if (*processSingle){
		//this path appears to double the affected frequency
		for (int x = 0; x < buffer.getNumSamples(); x++) {
			leftData[x] = filterAPeak.processSingleSampleRaw(leftData[x]);
			rightData[x] = filterAPeak.processSingleSampleRaw(rightData[x]);
		}
	}
	else
	{
		//This works as expected
		filterAPeak.processSamples(leftData, buffer.getNumSamples());
		filterAPeak.processSamples(rightData, buffer.getNumSamples());
	}
}

//==============================================================================
bool PkFilterTestAudioProcessor::hasEditor() const
{
    return false; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* PkFilterTestAudioProcessor::createEditor()
{
    return new PkFilterTestAudioProcessorEditor (*this);
}

//==============================================================================
void PkFilterTestAudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
}

void PkFilterTestAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new PkFilterTestAudioProcessor();
}
